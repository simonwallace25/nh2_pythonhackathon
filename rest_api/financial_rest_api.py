import logging
import datetime

from flask import Flask
from flask_restplus import Resource, Api
import pandas as pd
import pandas_datareader as pdr

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)


@api.route('/<string:tickerList>')
class Price(Resource):
    def get(self, tickerList):
        start_date = datetime.datetime.now() - datetime.timedelta(1)

        # split string into tickers
        list_tickers = tickerList.split(',')
        list_data = []

        for ticker in list_tickers:
            df = pdr.get_data_yahoo(ticker, start_date)
            df['Date'] = df.index

            list_data.append({'ticker': ticker,
                              'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
                              'open': df.iloc[-1]['Open'],
                              'close': df.iloc[-1]['Close'],
                              'change': df.iloc[-1]['Open'] - df.iloc[-1]['Close']})

        return list_data


@api.route('/advice/<string:ticker>/dates/<int:date_range>')
class Price(Resource):
    def get(self, ticker, date_range):
        start_date = datetime.datetime.now() - datetime.timedelta(date_range)
        
        date_range = 40

        df = pdr.get_data_yahoo(ticker, start_date)

        # Google - 2018
        df['mavg'] = df['Close'].rolling(window=date_range).mean()
        df['std'] = df['Close'].rolling(window=date_range).std()

        df['Upper Band'] = df['mavg'] + (df['std'] * 2)
        df['Lower Band'] = df['mavg'] - (df['std'] * 2)

        trade_recommendation = None

        if df['Close'][-1] > df['Upper Band'][-1]:
            trade_recommendation = 'BUY'
        elif df['Close'][-1] < df['Lower Band'][-1]:
            trade_recommendation = 'SELL'
        else:
            trade_recommendation = 'HOLD'

        return trade_recommendation


if __name__ == '__main__':
    app.run(debug=True)
